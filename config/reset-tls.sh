#!/bin/sh

ONLY_TRUST_KUBE_CA=${ONLY_TRUST_KUBE_CA:-false}
RESET_TLS=${RESET_TLS:-false}

should_rehash=false
for f in $(find /certs /run/secrets/kubernetes.io/serviceaccount -name '*.crt' 2>/dev/null)
do
    if test -s $f; then
	if ! test "$ONLY_TRUST_KUBE_CA" = false; then
	    if test -d /etc/pki/ca-trust/source/anchors; then
		if test -z "$had_reset"; then
		    rm -f /etc/pki/tls/cert.pem \
			/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		    had_reset=true
		fi
		cat $f >>/etc/pki/ca-trust/extracted/pem/tls-ca-bundle.pem
		cat $f >>/etc/pki/tls/cert.pem
	    else
		if test -z "$had_reset"; then
		    find /etc/ssl/certs -type f -delete
		    find /etc/ssl/certs -type l -delete
		    find /usr/share/ca-certificates -type f -delete
		    had_reset=true
		fi
		d=`echo $f | sed 's|/|-|g'`
		if ! cat $f >/usr/local/share/ca-certificates/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	else
	    if test -d /etc/pki/ca-trust/source/anchors; then
		dir=/etc/pki/ca-trust/source/anchors
	    else
		dir=/usr/local/share/ca-certificates
	    fi
	    d=`echo $f | sed 's|/|-|g'`
	    if ! test -s $dir/kube$d; then
		if ! cat $f >$dir/kube$d; then
		    echo WARNING: failed installing $f certificate authority >&2
		else
		    should_rehash=true
		fi
	    fi
	fi
    fi
done

if $should_rehash; then
    if test -d /etc/pki/ca-trust/source/anchors; then
	if ! update-ca-trust; then
	    echo WARNING: failed updating trusted certificate authorities >&2
	fi
    elif ! update-ca-certificates; then
	echo WARNING: failed updating trusted certificate authorities >&2
    fi
fi
unset should_rehash had_reset dir
