# k8s LibreOfficeOnline

LibreOfficeOnline image.

Build with:

```
$ make build
```

Start Demo in OpenShift:

```
$ make ocdemo
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description                    | Default                  |
| :------------------------- | --------------------------------- | ------------------------ |
|  `ADMIN_PASSWORD`          | LibreOfficeOnline Admin Password  | `secret`                 |
|  `ADMIN_USER`              | LibreOfficeOnline Admin Username  | `admin`                  |
|  `ALLOWED_LANGS`           | LibreOfficeOnline Allowed Locales | `en_US fr_FR`            |
|  `EXTRA_PARAMS`            | LibreOfficeOnline Extra Params    | undef                    |
|  `LOOL_DOMAIN`             | LibreOfficeOnline FQDN            | `lool.demo.local`        |
|  `LOOL_LOG_LEVEL`          | LibreOfficeOnline Log Level       | `information`            |
|  `MAX_FILE_SIZE`           | LibreOfficeOnline Max File Size   | `0`                      |
|  `NEXTCLOUD_DOMAIN`        | NextCould FQDN                    | `nextcloud.demo.local`   |
|  `PROOF_KEY`               | LibreOfficeOnline Proof Key       | undef                    |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point               | Description                                          |
| :-------------------------------- | ---------------------------------------------------- |
|  `/etc/loolwsd/proof_key`         | LibreOfficeOnline optional proof key                 |
|  `/etc/loolwsd/server.crt`        | LibreOfficeOnline optional custom server certificate |
|  `/etc/loolwsd/server.key`        | LibreOfficeOnline optional custom server key         |
|  `/etc/loolwsd/server-chain.crt`  | LibreOfficeOnline optional custom server chain       |
|  `/opt/lool/child-roots`          | Temporary directory                                  |
