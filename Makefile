SKIP_SQUASH?=1
FRONTNAME=opsperator
IMAGE=opsperator/lool
-include Makefile.cust

.PHONY: build
build:
	SKIP_SQUASH=$(SKIP_SQUASH) hack/build.sh

.PHONY: test
test:
	@@docker rm -f testlool || echo ok
	@@docker run --name testlool \
	    --cap-add=SYS_ADMIN \
	    --cap-add=MKNOD \
	    -e ADMIN_PASSWORD=secr3t \
	    -e ADMIN_USER=adm1n \
	    -e DEBUG=yay \
	    -e EXTRA_PARAMS=--o:ssl.enable=false \
	    -e LOOL_DOMAIN=lool.demo.local \
	    -e NEXTCLOUD_DOMAIN=cloud.demo.local \
	    -it $(IMAGE)

.PHONY: run
run:
	@@MAINDEV=`ip r | awk '/default/' | sed 's|.* dev \([^ ]*\).*|\1|'`; \
	MAINIP=`ip r | awk "/ dev $$MAINDEV .* src /" | sed 's|.* src \([^ ]*\).*$$|\1|'`; \
	docker run -e -e DEBUG=yayy \
	    -p 9980:9980 $(IMAGE)

.PHONY: kubebuild
kubebuild: kubecheck
	@@for f in image git task pipeline pipelinerun; \
	    do \
		kubectl apply -f deploy/kubernetes/tekton-$$f.yaml; \
	    done

.PHONY: kubecheck
kubecheck:
	@@kubectl version >/dev/null 2>&1 || exit 42

.PHONY: ocbuild
ocbuild: occheck
	@@oc process -f deploy/openshift/imagestream.yaml | oc apply -f-
	@@BRANCH=`git rev-parse --abbrev-ref HEAD`; \
	if test "$$GIT_DEPLOYMENT_TOKEN"; then \
	    oc process -f deploy/openshift/build-with-secret.yaml \
		-p "GIT_DEPLOYMENT_TOKEN=$$GIT_DEPLOYMENT_TOKEN" \
		-p "LOOL_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	else \
	    oc process -f deploy/openshift/build.yaml \
		-p "LOOL_REPOSITORY_REF=$$BRANCH" \
		| oc apply -f-; \
	fi

.PHONY: occheck
occheck:
	@@oc whoami >/dev/null 2>&1 || exit 42

.PHONY: occlean
occlean: occheck
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true
	@@oc process -f deploy/openshift/secret.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc delete -f- || true

.PHONY: ocdemoephemeral
ocdemoephemeral: ocbuild
	@@if ! oc describe secret lool-$(FRONTNAME) >/dev/null 2>&1; then \
	    oc process -f deploy/openshift/secret.yaml \
		-p FRONTNAME=$(FRONTNAME) | oc apply -f-; \
	fi
	@@oc process -f deploy/openshift/run-ephemeral.yaml \
	    -p FRONTNAME=$(FRONTNAME) | oc apply -f-

.PHONY: ocdemo
ocdemo: ocdemoephemeral

.PHONY: ocpurge
ocpurge: occlean
	@@oc process -f deploy/openshift/build.yaml | oc delete -f- || true
	@@oc process -f deploy/openshift/imagestream.yaml | oc delete -f- || true
