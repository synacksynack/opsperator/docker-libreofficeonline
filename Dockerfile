FROM docker.io/libreoffice/online:latest

# LibreOfficeOnline image for OpenShift Origin

LABEL io.k8s.description="LiberOfficeOnline." \
      io.k8s.display-name="LibreOfficeOnline" \
      io.openshift.expose-services="9980:http" \
      io.openshift.tags="libreofficeonline,lool" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-libreofficeonline" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="1.0"

ENV DEBIAN_FRONTEND=noninteractive \
    ENC=UTF-8 \
    LANG=en_US.UTF-8 \
    LANGUAGE=en_US.UTF-8 \
    LC_CTYPE=en_US.UTF-8 \
    LOOL_STATIC_ASSETS=/usr/share/loolwsd/loleaflet/dist

USER root
COPY config/* /

RUN set -x \
    && echo "# Install Dumb-init" \
    && apt-get update \
    && apt-get -y install dumb-init \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && if test "$DEBUG"; then \
	echo "# Install Debugging Tools" \
	&& apt-get -y install vim; \
    fi \
    && echo "# Install Tools" \
    && apt-get install -y curl wget libnss-wrapper ca-certificates tzdata \
	locales openssh-client \
    && echo "# Reconfigures Packages" \
    && dpkg-reconfigure -f noninteractive tzdata \
    && sed -i -e 's|# en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|' \
	-e 's|# $LANG $ENC|$LANG $ENC|' \
	-e 's|# pt_PT ISO-8859-1|pt_PT ISO-8859-1|' \
	-e 's|# pt_PT.UTF-8 UTF-8|pt_PT.UTF-8 UTF-8|' \
	-e 's|# de_DE ISO-8859-1|de_DE ISO-8859-1|' \
	-e 's|# de_DE.UTF-8 UTF-8|de_DE.UTF-8 UTF-8|' \
	-e 's|# es_ES ISO-8859-1|es_ES ISO-8859-1|' \
	-e 's|# es_ES.UTF-8 UTF-8|es_ES.UTF-8 UTF-8|' \
	-e 's|# fr_FR ISO-8859-1|fr_FR ISO-8859-1|' \
	-e 's|# fr_FR.UTF-8 UTF-8|fr_FR.UTF-8 UTF-8|' /etc/locale.gen \
    && echo 'LANG="$LANG"' >/etc/default/locale \
    && locale-gen \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale \
    && mv /reset-tls.sh /nsswrapper.sh /usr/local/bin/ \
    && echo "# Fixing permissions" \
    && for d in $LOOL_STATIC_ASSETS/branding /etc/loolwsd /opt/lool /etc/ssl \
	    /var/cache/loolwsd /opt/libreoffice/share/uno_packages/cache \
	    /usr/share/ca-certificates /usr/local/share/ca-certificates; \
	do \
	    mkdir -p $d \
	    && chown -R 1001:root $d \
	    && chmod -R g=u $d; \
	done \
    && chown 1001:root /etc/passwd \
    && echo "# Configure LibreOfficeOnline" \
    && mv /etc/loolwsd/loolwsd.xml /etc/loolwsd/loolwsd.xml.orig \
    && touch $LOOL_STATIC_ASSETS/branding.css \
    && touch $LOOL_STATIC_ASSETS/branding.js \
    && echo "# Cleaning Up" \
    && apt-get clean \
    && rm -rf /usr/share/man /usr/share/doc /var/lib/apt/lists/* \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ENTRYPOINT [ "dumb-init","--","/run-lool.sh" ]
USER 1001
