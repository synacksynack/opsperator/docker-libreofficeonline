#!/bin/sh

if test -s /tmp/lool-passwd; then
    echo Skipping nsswrapper setup - already initialized
elif test "`id -u`" -ne 0; then
    echo Setting up nsswrapper mapping `id -u` to lool
    if grep ^lool: /etc/passwd >/dev/null; then
	sed "s|^lool:.*|lool:x:`id -g`:|" /etc/group >/tmp/lool-group
	sed \
	    "s|^lool:.*|lool:x:`id -u`:`id -g`:lool:/etc/lool:/usr/sbin/nologin|" \
	    /etc/passwd >/tmp/lool-passwd
    else
	(
	    cat /etc/passwd
	    echo "lool:x:`id -u`:`id -g`:lool:/etc/lool:/sbin/nologin"
	) >/tmp/lool-passwd
    fi
    cat /tmp/lool-passwd >/etc/passwd
    export NSS_WRAPPER_PASSWD=/tmp/lool-passwd
    export NSS_WRAPPER_GROUP=/tmp/lool-group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
fi
