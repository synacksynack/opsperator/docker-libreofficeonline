#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh
. /usr/local/bin/reset-tls.sh

ADMIN_PASSWORD="${ADMIN_PASSWORD:-secret}"
ADMIN_USER="${ADMIN_USER:-admin}"
ADMINTOGGLE=false
ALLOWED_LANGS="${ALLOWED_LANGS:-en_US fr_FR}"
CA=
CERT=
CONFIG=/etc/loolwsd/loolwsd.xml
KEY=
LOOL_DOMAIN=${LOOL_DOMAIN:-lool.demo.local}
MAX_FILE_SIZE=${MAX_FILE_SIZE:-0}
NEXTCLOUD_DOMAIN=${NEXTCLOUD_DOMAIN:-nextcloud.demo.local}
SSLTOGGLE=false

test -z "$LOOL_STATIC_ASSETS" && LOOL_STATIC_ASSETS=/usr/share/loolwsd/loleaflet/dist

cp /etc/resolv.conf /etc/hosts /opt/lool/systemplate/etc/
if test -s /etc/loolwsd/server.crt -a -s /etc/loolwsd/server.key; then
    if ! test -s /etc/loolwsd/server-chain.crt; then
	if test -s /run/secrets/kubernetes.io/serviceaccount/service-ca.crt; then
	    cat /run/secrets/kubernetes.io/serviceaccount/service-ca.crt \
		>/etc/loolwsd/server-chain.crt
	else
	    touch /etc/loolwsd/server-chain.crt
	fi
    fi
    CA=/etc/loolwsd/server-chain.crt
    CERT=/etc/loolwsd/server.crt
    KEY=/etc/loolwsd/server.key
    SSLTOGGLE=true
fi
if test "$PROOF_KEY"; then
    echo "$PROOF_KEY" >/etc/loolwsd/proof_key
elif ! test -s /etc/loolwsd/proof_key; then
    echo WARNING: generating ephemeral ProofKey -- consider defining one
    ssh-keygen -t rsa -N "" -f /etc/loolwsd/proof_key >/dev/null 2>&1
fi
if test "$ADMIN_USER" -a "$ADMIN_PASSWORD"; then
    ADMINTOGGLE=true
fi
for l in $ALLOWED_LANGS
do
    if grep "^$l" /etc/locale.gen >/dev/null; then
	_ALLOWED_LANGS="$_ALLOWED_LANGS $l"
    fi
done

sed -e "s|LOOL_HOST|$LOOL_DOMAIN|" \
    -e "s|NC_HOST|$NEXTCLOUD_DOMAIN|" \
    -e "s|MAX_FILE_SIZE|$MAX_FILE_SIZE|" \
    -e "s|CA|$CA|" -e "s|CERT|$CERT|" -e "s|KEY|$KEY|" \
    -e "s|ADMINTOGGLE|$ADMINTOGGLE|" \
    -e "s|ADMIN_PASSWORD|$ADMIN_PASSWORD|" \
    -e "s|ADMIN_USER|$ADMIN_USER|" \
    -e "s|ALLOWED_LANGS|$_ALLOWED_LANGS|" \
    -e "s|SSLTOGGLE|$SSLTOGGLE|" /config.xml >$CONFIG

if echo "$LOOL_LOG_LEVEL" | grep -E \
	'^(fatal|critical|error|warning|notice|information|debug|trace)$' \
	>/dev/null; then
    sed -i "s|>warning</level>|>$LOOL_LOG_LEVEL</level>|" $CONFIG
elif test "$DEBUG"; then
    sed -i 's|>warning</level>|>debug</level>|' $CONFIG
else
    sed -i 's|>warning</level>|>information</level>|' $CONFIG
fi
if ! test -s $LOOL_STATIC_ASSETS/branding/branding.css; then
    cat $LOOL_STATIC_ASSETS/branding.css \
	>$LOOL_STATIC_ASSETS/branding/branding.css
fi
if ! test -s $LOOL_STATIC_ASSETS/branding/branding.js; then
    cat $LOOL_STATIC_ASSETS/branding.js \
	>$LOOL_STATIC_ASSETS/branding/branding.js
fi

unset CA CERT KEY SSLTOGGLE ADMINTOGGLE LOOL_STATIC_ASSETS CONFIG DEBUG \
    LOOL_DOMAIN NEXTCLOUD_DOMAIN MAX_FILE_SIZE OC_CA ADMIN_USER ADMIN_PASSWORD

exec /usr/bin/loolwsd --version \
    --o:sys_template_path=/opt/lool/systemplate \
    --o:lo_template_path=/opt/libreoffice \
    --o:child_root_path=/opt/lool/child-roots \
    --o:file_server_root_path=/usr/share/loolwsd \
    $EXTRA_PARAMS
